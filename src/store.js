import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const moduleA = {
  namespaced: true,
  state: {
    moduleFoo: 'moduleFoo',
  },
  mutations: {
    mMutation(state, value) {
      state.moduleFoo = value;
    },
  },
  getters: {
    otherGetter(state) {
      return `${state.moduleFoo} other`;
    },
    mGetter(state, getters) {
      return id => `${state.moduleFoo.concat(id)} ${getters.otherGetter}`;
    },
  },
  actions: {
    mAction({
      rootState, commit,
    }, payload) {
      commit('mMutation', `${rootState.count} + ${payload}`);
    },
  },
};

export default new Vuex.Store({
  state: {
    count: 0,
  },
  mutations: {
    increment(state) {
      state.count += 1;
    },
  },
  getters: {
    countTimesTwo({ count }) {
      return count * 2;
    },
    countTimesX: ({ count }) => x => count * x,
  },
  modules: { a: moduleA },
});
