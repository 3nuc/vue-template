import Vue from 'vue';

const ConsoleLog = {
  install(_Vue) {
    // eslint-disable-next-line
    _Vue.prototype.$_log = console.log;
  },
};

Vue.use(ConsoleLog);
