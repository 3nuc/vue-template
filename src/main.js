import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.prototype.$_log = console.log;
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
