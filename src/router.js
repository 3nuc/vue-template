import VueRouter from 'vue-router';
import Vue from 'vue';
import Home from './views/Home.vue';

const router = new VueRouter({
  routes: [
    {
      path: '/',
      components: {
        default: { template: '<div>asd</div>' },
        footer: { template: '<div>footer</div>' },
      },
    },
    {
      path: '/home/:id',
      component: Home,
      children: [
        {
          path: '',
          components: {
            default: { template: '<div>homenested</div>' },
            foo: { template: '<div>foo</div>' },
          },
        },
      ],
    },
  ],
});

Vue.use(VueRouter);

export default router;
